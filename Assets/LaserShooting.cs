﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShooting : MonoBehaviour {

    public GameObject bullet;
    public float fireTime = 0.30f;
    float coolDownTimer = 0;
    public Vector3 bulletOffset = new Vector3(0,1f,0);
    float invlTimer = .2f;
    int correctLayer;

	// Use this for initialization
	void Start () {
        correctLayer = gameObject.layer;
    }

    // Update is called once per frame
    void Update () {
        coolDownTimer -= Time.deltaTime;

        if (Input.GetButton("Fire1") && coolDownTimer <= 0){
            //Shoot laser bullet
            gameObject.layer = 10;
            Debug.Log("Pew!");
            coolDownTimer = fireTime;
            Vector3 offset = bulletOffset;
            Instantiate(bullet, transform.position + offset, transform.rotation);
            invlTimer -= Time.deltaTime;
            if (invlTimer <= 0)
            {
                gameObject.layer = correctLayer;
            }
        }

	}
}
