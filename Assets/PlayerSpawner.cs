﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    public GameObject playerPrefab;
    GameObject playerInstance;

    float respawnTimer;
    public int numLives = 3;

	// Use this for initialization
	void Start () {
        SpawnPlayer();
	}

    void SpawnPlayer(){
        numLives--;
        respawnTimer = 1;
        playerInstance = (GameObject)Instantiate(playerPrefab, transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update () {
        if(playerInstance == null){
            respawnTimer -= Time.deltaTime;

            if(respawnTimer <= 0){
                SpawnPlayer();
            }
        }
	}
}
