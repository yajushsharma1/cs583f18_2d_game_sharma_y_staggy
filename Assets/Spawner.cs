﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{


    public GameObject enemyAstroid1;
    public GameObject enemyAstroid2;
    public GameObject enemyAstroid3;

    float randx;
    Vector2 whereToSpawn;
    public float enemyRate = .025f;
    float nextEnemy = 0.0f;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextEnemy)
        {
            nextEnemy = Time.time + enemyRate;
            randx = Random.Range(-3.85f, 2.72f);
            whereToSpawn = new Vector2(randx, transform.position.y);
            Instantiate(enemyAstroid1, whereToSpawn, Quaternion.identity);
            ScoreScript.ScoreVal += 1;
        }

        if (Time.time > nextEnemy)
        {
            nextEnemy = Time.time + enemyRate;
            randx = Random.Range(-3.85f, 2.72f);
            whereToSpawn = new Vector2(randx, transform.position.y);
            Instantiate(enemyAstroid2, whereToSpawn, Quaternion.identity);
            ScoreScript.ScoreVal += 1;
        }

        if (Time.time > nextEnemy)
        {
            nextEnemy = Time.time + enemyRate;
            randx = Random.Range(-3.85f, 2.72f);
            whereToSpawn = new Vector2(randx, transform.position.y);
            Instantiate(enemyAstroid3, whereToSpawn, Quaternion.identity);
            ScoreScript.ScoreVal += 1;
        }
    }
}