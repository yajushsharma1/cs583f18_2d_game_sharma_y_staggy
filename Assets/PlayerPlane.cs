﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlane : MonoBehaviour
{
    public float Speed = 8f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Player control left or right using horizontal axis
        Vector3 pos = transform.position;
        pos.x += Input.GetAxis("Horizontal") * Speed * Time.deltaTime;
        transform.position = pos;

        //Setting bounds
        if (transform.position.x <= -4.68f)// left bound
        {
            transform.position = new Vector2(-4.68f, transform.position.y);
        }
        else if (transform.position.x >= 4.14f)// right bound
        {
            transform.position = new Vector2(4.14f, transform.position.y);
        }

    }
}